package tools;

import com.sun.tools.attach.VirtualMachine;
import gui.AttachedGUI;

import java.io.IOException;

public class Attacher {
	
	private static final String PATH = Attacher.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	
	private static VirtualMachine vm;

	public static void attach(int pid) throws Exception {
		vm = VirtualMachine.attach(String.valueOf(pid));
		vm.loadAgent(PATH.substring(1));
		AttachedGUI.getInstance().draw();
	}
	
	public static void detach() throws IOException {
		vm.detach();
	}

}