package tools;

import java.awt.Image;

import javax.swing.ImageIcon;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public final class IconImage {
	
	private static final String icon = 
			"iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA" +
			"IGNIUk0AAHolAACAgwAA+f8AAIDoAABSCAABFVgAADqXAAAXb9daH5AAAAfHSURBVHjavJd9bF1l" +
			"Hcc/z3POua/tbddu3dqtsA7GNlvHJtGNObZkMHkJCIoJ/qEGY2L8Q4kkSIj+aQxRISKJaGKMkihZ" +
			"Iioh4gu6jTnoWmQM98LKxtZtve3t+nJ7e+655/08j3+029r1bgNM/CXPTc7Jk/v9nu/v+/s9z09o" +
			"rZkXy1qoGwreGJ+6/sfwQADPf2XLLRXTMKhGEcucGncePcFTwD+BVekUd2+5haoXcLX48oGDmAve" +
			"rl+9cKcANLmeY4O7vjUyvnlvNvO5UrX2QltDbpchpa2F4KPGQgKJqrNNg2XuaPrs1s1bhsbZs/ft" +
			"7X1HBrZ3NDbclMpmHtvkekgggmwEptJUxUcl8Os9by34eBMwIFqfTTHesZTQdVkDmK47nUsSPt3R" +
			"inCzH98xMvFHCekpy/yGEmKPlNIHfVUC4nIPHDaNec8lpfm91gQgO+Hzmw25q5woQ8CeJJ+/b2/r" +
			"IvcXW9aRX9z0V8r2XYO9x3hqooKyzINr25fukAJbX4HDI8feW6jA+svyuURofqchA+qeVPpFo3XR" +
			"i39BP2Qubf/6sXLZPaDU4hte2j/xbYPOwrZP8JIU+E6NNNwymahm05D21URY6IE4mf8I2Tx85hy8" +
			"+epNq0rj27b/9F9vvvVQHMf3eun0+tYgeOBnLS37/jNZ/seX/t6XS2ndtREwpHw5HUVFHYEPxEIg" +
			"PkgKeOiOi77DkEZ8dvQPgwcH7t8rxNBwW+uL7oaNp3ZPVp4tl0rSSqcRgB8ElJOk+rWqXdtpsOx1" +
			"N9zflE7tLEJwSkiKUhKnUlxeLMeLwwsViN4dvEhACLHWXHvd/avX3cWrf+7tHBgqPZpSkuyaNUjD" +
			"oLGxkSRJMC0Lw/MaX1nS1jh64ypeO/puWsXxGj+OD6M1JpASAuqU6wICPzl6em7x6U1HT7GsfTHF" +
			"qWm6gRNTU2QaG7muqwvXddFhSFirkU6nMS2LKcNCJsmnhNZvZ4V4Vmv9JDBufFAP7M3n5za/4/0N" +
			"DV/4ZK7hufXRRFsZCDduxDRNhGVhmibj589TKBQwLYsoijCEwJASM502wiB4VCXJFzU8K4R4BiH8" +
			"axLY+Pjj8zpQYprNZc9fZL7wW3qTmFIux4pslgnbZmxkhJVdXcRKEQYBYRgSRxFty5YRRxHVapU4" +
			"jttVkvxAa/0btB69JoF8U9M8PpZl/Xz/ob1W/8e6aW9ro/zGG7Tk84wOD7Pu5ptRSqGShAnXpeY4" +
			"uJ6HNE1C1yWJY+IoAigB1XopkAtMGIYXF/DM6MiINXz2LHfv3MkNXV2k0mkGjh5l87ZtNBYKZLJZ" +
			"HNumOj3N2u5uDNMkCgLsSoVw5j8AqkIIVwjBhSWlrK/A9PT0DDMh7mgsFLb17d/P5ttuw7QsGgoF" +
			"Vq5aRfvy5TS1tlKr1SgVi4yPjXFTTw8qSajZNmOjo2itEZdcP6S11hfAkyQhDIL6BMoTEwCkM5nH" +
			"S8UiUgg6VqwgjmMGjhzBME3WdHfjOA5JHHPqvffYtHUr0jB49513GDpzBmkYc8ERQpwTQhAGAZ7n" +
			"4XseSZJcgUC5jIC1uXx+x/HDh+nZsIFKpYKUkiOHDnHvgw8SxzGpVIre117j1q1bae/s5FB/P6dP" +
			"nsS0rPmdbgZ4vGrbc1NyZRO6joOQ8j7HcQylFEJKpisVBk+coHPlSsxUCqU1vfv20bJ4Md0bNnBq" +
			"YIB/9/VhmGadXisIgiAMw1DOVPY1TOi5biYKw1trtk0un6dq25QnJxkpFlna0UGlXGZ0ZITiuXNs" +
			"u/12As9j3+7dRFE0T3aAC21eKZUFVgNtl2MuoOz7/hIdRdfb5TJtnZ34vk9leJhMLofruiAEA0eO" +
			"sGbdOjRwsK+PobNnSWcyl4CVwjBNpJQopdBKAXTNrhJwDpgG1AIFfNc1Pc83U6kMseMQhCGVqSka" +
			"CwWmKxVKw8NMlcu0tLVRPHOG/t5erFTqUvdUisamJgrNzRdPPzUjxSJgKdAzq4ZVNwWu64VKx/6q" +
			"jhxBoqiNjYFSpHM5fN+nVCxSaG6mVqtx9NAhHMe5WNMAqVSKQlMTdqVCFEVzFWiexRsETgJhfQ94" +
			"Yeh5gffN7ZN0Nvm4yqCQayAYHSWMInzfJ5vPMzYywqn338ecYzytNcbs+RBHEckMMErrCBgHXgfe" +
			"BKZmD/yFHqhMTgSWWBR0pA1MIWjJp2hKC4anFeb4OEkYYlkW46UScRQhDWNeyUVhSBLHqFkfaKWq" +
			"KkkOAH3A5DWrIAxc3665pedejTk34tJRcPnenZOgYwJMslaG2tgYlUql7vmulEJrjWlZxHF8YHJy" +
			"8h6l1Cv1wOtfySB0piuP/fJ1dC7f8PANS6q0GiG+Z7F6uURqyenJgMD3kXUICCGQhlGr2vYPa47z" +
			"NOBe7VYs673UWk8EztRXq+XzD/efjqee/pvAdnxuXOLxyE4Hx5uR+XIFhJT4vr97YmxsR81xvn8t" +
			"8CspcOlCGoXPDw6dP7bLafsVRrLe8mwsTxBFuYvj0oWv1lq7dqXyZM1xfnTB4R8kFlxKRf0xK5/K" +
			"5L7b09XyxKbrtHz5uDmrwMz+wPf325XKd+I47v8wY5nWeuZn7roqW8Pa2dzWcbJ9+XLdsWKFXtre" +
			"Pp3L558A0h9lLvzQBGYVWlxobt61qLX1T4Zh9PA/hNa6zlzwf47/DgBMtPuDPwGcRQAAAABJRU5E" +
			"rkJggg==";
	
	public static Image getImage() {
		return new ImageIcon(Base64.decode(icon)).getImage();
	}

}
