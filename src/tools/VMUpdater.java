package tools;

import sun.jvmstat.monitor.MonitoredHost;
import sun.jvmstat.monitor.event.HostEvent;
import sun.jvmstat.monitor.event.HostListener;
import sun.jvmstat.monitor.event.VmStatusChangeEvent;

public class VMUpdater {
	
	public static MonitoredHost MH;

	public VMUpdater() {
		try {
			MH = MonitoredHost.getMonitoredHost("localhost");
			MH.addHostListener(new HostListenerAction());
		} catch (Exception e) {}
	}

	private class HostListenerAction implements HostListener {

		@Override
		public void vmStatusChanged(VmStatusChangeEvent vmStatusChangeEvent) {
			if(vmStatusChangeEvent.getStarted().size() > 0)
				VMList.add(vmStatusChangeEvent.getStarted());
			if(vmStatusChangeEvent.getTerminated().size() > 0)
				VMList.remove(vmStatusChangeEvent.getTerminated());
		}

		@Override
		public void disconnected(HostEvent hostEvent) {}
	}

}
