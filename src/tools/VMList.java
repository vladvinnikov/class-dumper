package tools;

import gui.GUI;
import sun.jvmstat.monitor.*;

import java.lang.management.ManagementFactory;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class VMList extends CopyOnWriteArrayList<Integer> {
	
	private static final String RUNTIME_NAME = ManagementFactory.getRuntimeMXBean().getName();
	private static final int PROCESS_ID = Integer.parseInt(RUNTIME_NAME.substring(0, RUNTIME_NAME.indexOf("@")));
	
	private static VMList list = new VMList();
	
	@Override
	public boolean add(Integer vmd) {
		String name = String.valueOf(vmd);
		try {
			VmIdentifier vmi = new VmIdentifier("//" + vmd + "?mode=r");
			MonitoredVm vm = null;
			vm = VMUpdater.MH.getMonitoredVm(vmi);
			name = name + " " + MonitoredVmUtil.mainClass(vm, false);
		} catch(Exception e) {
			return false;
		}
		GUI.getInstance().addVM(name);
		return super.add(vmd);
	}
	
	@Override
	public boolean remove(Object key) {
		GUI.getInstance().removeVM((Integer) key);
		return super.remove(key);
	}
	
	public static int getVMD(String key) {
		for(int dscr : list)
			if(Integer.parseInt(key.substring(0, key.indexOf(" "))) == dscr)
				return dscr;;
		return -1;
	}

	public static void add(Set<Integer> dscr) {
		for(int pid : dscr)
			if(PROCESS_ID != pid)
				list.add(pid);
	}

	public static void remove(Set<Integer> dscr) {
		for(int pid : dscr)
			list.remove((Object) pid);
	}
	
}
