package tools;

import gui.AttachingWindow;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.SwingUtilities;

public class CommandExecutor extends Thread {

	private CE mode;
	private String loc;
	private String jarName;
	private boolean pack;
	private List<String> classes;
	private String jarPath;

	public CommandExecutor(CE e, String path, String jarName,
			boolean pack, String jarPath,  List<String> classes) {
		setVis(true);
		mode = e;
		this.loc = path;
		this.classes = classes;
		this.jarPath = jarPath;
		this.jarName = jarName;
		this.setDaemon(true);
		this.pack = pack;
		this.start();
	}

	@Override
	public void run() {
		switch (mode) {
		case JAR:
			try {
				AgentRMI.getInstance().dumpJar(loc, jarPath);
			} catch (RemoteException e1) {
			}
			try {
				if (pack)
					makeJar();
			} catch (Exception e) {
			}
			break;
		case CLASSES:
			
			if (classes == null || classes.size() == 0) {
				classes = new ArrayList<String>();
				classes.add(".*");
			} else if (classes.contains("*")) {
				classes = new ArrayList<String>();
				classes.add(".*");
			}
			try {
				AgentRMI.getInstance().dump(classes, loc);
				try {
					if (pack)
						makeJar();
				} catch (Exception e) {
				}
			} catch (RemoteException e1) {
			}
			break;
		}
		
		if(pack)
			deleteDirectory(new File(loc));
		setVis(false);
	}

	private boolean makeJar() throws IOException {
		File directory = new File(loc);
		if (!directory.exists())
			return true;
		URI base = directory.toURI();
		Deque<File> queue = new LinkedList<File>();
		queue.push(directory);
		OutputStream out = new FileOutputStream(new File(
				directory.getAbsolutePath() + File.separator + jarName));
		Closeable res = out;

		try {
			ZipOutputStream zout = new ZipOutputStream(out);
			res = zout;
			while (!queue.isEmpty()) {
				directory = queue.pop();
				for (File child : directory.listFiles()) {
					String name = base.relativize(child.toURI()).getPath();
					if (child.isDirectory()) {
						queue.push(child);
						name = name.endsWith("/") ? name : name + "/";
						zout.putNextEntry(new ZipEntry(name));
					} else {
						if (name.endsWith(".jar"))
							continue;
						zout.putNextEntry(new ZipEntry(name));
						InputStream in = new FileInputStream(child);
						try {
							byte[] buffer = new byte[1024];
							while (true) {
								int readCount = in.read(buffer);
								if (readCount < 0) {
									break;
								}
								zout.write(buffer, 0, readCount);
							}
						} finally {
							in.close();
						}
						zout.closeEntry();
					}
				}
			}
		} finally {
			res.close();
		}
		return true;
	}

	public static enum CE {
		CLASSES, JAR
	}

	private void setVis(final boolean b) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				AttachingWindow.getInstance().setVisible(b);
			}
		});
	}
	
	private static void deleteDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				File f = new File(dir, children[i]);
				deleteDirectory(f);
			}
			dir.delete();
		} else if(dir.getName().endsWith("jar"))
				return;
		else
			dir.delete();
	}

}
