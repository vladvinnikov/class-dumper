package tools;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

import classdumper.RemoteInterface;

public class AgentRMI extends Thread implements RemoteInterface {
	
	private RemoteInterface rmi;
	
	private static AgentRMI _instance;
	public static AgentRMI getInstance() {
		return _instance == null ? _instance = new AgentRMI() : _instance;
	}
	private Runnable actionAfter;

	private AgentRMI() {
		this.setDaemon(true);
	}
	
	public void connect(Runnable afterAction) {
		this.actionAfter = afterAction;
		this.start();
	}
	
	@Override
	public void run() {
		while(true)
		{
			try {				 
				Registry registry = LocateRegistry.getRegistry("127.0.0.1", 8888);
				 rmi = (RemoteInterface) registry.lookup("RemoteInterface");
				 actionAfter.run();
				 break;
			} catch(Exception e) {
				e.printStackTrace();
				System.out.println("Error while connection" + e.getMessage() + ". Try to reconnect");
				try{Thread.sleep(5000L);}catch(Exception e1) {}
			}
		}
	}
	
	@Override
	public String[] getLoadedClasses() throws RemoteException {
		return rmi.getLoadedClasses();
	}

	@Override
	public boolean dump(List<String> classesPattern, String path) throws RemoteException {
		return rmi.dump(classesPattern, path);
	}

	@Override
	public boolean dumpJar(String path, String dumpPath) throws RemoteException {
		return rmi.dumpJar(path, dumpPath);
	}
}
