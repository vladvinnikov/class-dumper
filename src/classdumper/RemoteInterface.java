package classdumper;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RemoteInterface extends Remote {
	
	public String[] getLoadedClasses() throws RemoteException;
	public boolean dump(List<String> classesPattern, String path) throws RemoteException;
	public boolean dumpJar(String path, String dumpPath) throws RemoteException;

}
