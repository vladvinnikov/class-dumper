package classdumper;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class RemoteInterfaceImpl extends UnicastRemoteObject implements RemoteInterface {
	
	protected RemoteInterfaceImpl() throws RemoteException {
		super();
	}
	
	@Override
	public String[] getLoadedClasses() throws RemoteException {
		return ClassDumperAgent.getLoadedClasses();
	}
	
	@Override
	public boolean dump(List<String> classesName, String path) throws RemoteException {
		return ClassDumperAgent.dumpClasses(classesName, path);
	}

	@Override
	public boolean dumpJar(String path, String jarPath) throws RemoteException {
		try {
			return ClassDumperAgent.dumpJar(path, jarPath);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
