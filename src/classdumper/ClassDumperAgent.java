package classdumper;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;

public class ClassDumperAgent implements ClassFileTransformer {
	
	private static String dumpDir = "C:/dump";
	private static Pattern classes;
	private static Instrumentation inst;
	
	private static List<Class<?>> jarCandidate = new ArrayList<Class<?>>();
	private static JarClassLoader cl = new JarClassLoader(ClassLoader.getSystemClassLoader());
	
	private static RemoteInterfaceImpl rmi;

	public static void premain(String agentArgs, Instrumentation inst) {
		agentmain(agentArgs, inst);
	}

	public static void agentmain(String agentArgs, Instrumentation i) {
		try {
			rmi = new RemoteInterfaceImpl();
		} catch (RemoteException e) {
			System.out.println("Error while starting connection service!");
			e.printStackTrace();
		}
		try {
			Registry registry = LocateRegistry.createRegistry(8888);
			registry.bind("RemoteInterface", rmi);
		} catch (Exception e) {}
		System.out.println("Class Dumper agent loaded");
		inst = i;
		inst.addTransformer(new ClassDumperAgent(), true);
	}
	
	private static void doDump() {
		try { deleteDirectory(new File(dumpDir)); } catch(Exception e) {}
		Class<?>[] classes = inst.getAllLoadedClasses();
		List<Class<?>> candidates = new ArrayList<Class<?>>();
		for (Class<?> c : classes)
			if (isCandidate(c.getName()) && inst.isModifiableClass(c))
				candidates.add(c);
		try {
			if (!candidates.isEmpty())
				inst.retransformClasses(candidates.toArray(new Class[0]));
		} catch (UnmodifiableClassException uce) {
		}
	}

	@Override
	public byte[] transform(ClassLoader loader, String className,
			Class<?> redefinedClass, ProtectionDomain protDomain, byte[] classBytes) {
		if (isCandidate(className) || isJarCandidate(className))
			dumpClass(className, classBytes);

		return null;
	}

	private static boolean isCandidate(String className) {
		className = className.replace('/', '.');
		if(className.contains("classdumper") || classes == null)
			return false;
		return className == null ? false : classes.matcher(className).matches();
	}
	
	private static boolean isJarCandidate(String className) {
		className = className.replace('/', '.');
		if(className.contains("classdumper"))
			return false;
		for(Class<?> c : jarCandidate)
			if(className.equals(c.getName()))
				return true;
		return false;
	}

	private static void dumpClass(String className, byte[] classBuf) {
		try {
			className = className.replace("/", File.separator);
			StringBuilder buf = new StringBuilder();
			buf.append(dumpDir);
			buf.append(File.separatorChar);
			int index = className.lastIndexOf(File.separatorChar);
			if (index != -1)
				buf.append(className.substring(0, index));
			String dir = buf.toString();
			new File(dir).mkdirs();

			String fileName = dumpDir + File.separator + className + ".class";
			FileOutputStream fos = new FileOutputStream(fileName);
			fos.write(classBuf);
			fos.close();
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}
	
	public static boolean dumpClasses(List<String> classesPattern, String path) {
		dumpDir = path;
		for(String ptn : classesPattern) {
			classes = Pattern.compile(ptn);
			System.out.println("Dumping classes: " + classes.pattern() + ";");
			doDump();
		}
		classes = null;
		return true;
	}
	
	public static boolean dumpJar(String path, String jarPath) throws Exception {
		dumpDir = path;
		deleteDirectory(new File(path));
		System.out.println("Dumping jar " + jarPath);
		jarCandidate.clear();
		Hashtable<String, Class<?>> classes = cl.loadJar(jarPath, dumpDir);
		for(Class<?> c : classes.values())
			jarCandidate.add(c);
		if (!jarCandidate.isEmpty())
			inst.retransformClasses(jarCandidate.toArray(new Class[jarCandidate.size()]));
		return true;
	}
	
	public static String[] getLoadedClasses() {
		List<String> list = new ArrayList<String>();
		for(Class<?> clazz : inst.getAllLoadedClasses()) {
			if(clazz.getName().startsWith("java.") || clazz.getName().startsWith("sun.") ||
					clazz.getName().startsWith("com.sun.") || clazz.getName().startsWith("[") || clazz.getName().startsWith("javax."))
				continue;
			list.add(clazz.getName());
		}
		return list.toArray(new String[list.size()]);
	}
	
	private static void deleteDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				File f = new File(dir, children[i]);
				deleteDirectory(f);
			}
			dir.delete();
		} else if(dir.getName().endsWith("jar"))
				return;
		else
			dir.delete();
	}
	
}