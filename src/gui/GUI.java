package gui;

import tools.Attacher;
import tools.IconImage;
import tools.VMList;
import tools.VMUpdater;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUI extends JFrame {
	
	private static GUI _instance;
	public static GUI getInstance() {
		return _instance == null ? _instance = new GUI() : _instance;
	}
	
	public static final Dimension resolution = Toolkit.getDefaultToolkit().getScreenSize();
	public static Dimension minimumSize = new Dimension();
	
	private DefaultListModel<String> vm_processes_md = new DefaultListModel<String>();
	private JList<String> vm_processes = new JList<String>(vm_processes_md);
	
	private JPanel processPanel = new JPanel();
	private JButton attach = new JButton("Attach");
	
	private AttachingWindow aw = new AttachingWindow(this);
	
	private GUI() {
		initializeGUI();
		initializeListeners();
	}
	
	private void initializeGUI() {
		this.setResizable(false);
		this.setIconImage(IconImage.getImage());
		this.setLayout(new BorderLayout(5, 5));
		//this.setBackground(new Color(50));
		this.setName("JCD");
		this.setTitle("Java Class Dumper");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		processPanel.add(new JScrollPane(vm_processes));
		processPanel.setBorder(new TitledBorder("Java Virtual Machines"));
		
		vm_processes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		vm_processes.setLayoutOrientation(JList.VERTICAL);
		vm_processes.setVisibleRowCount(9);
		vm_processes.setFixedCellHeight(17);
		
		this.add(processPanel, BorderLayout.NORTH);
		this.add(attach, BorderLayout.CENTER);
		this.packAndResize();
		this.setVisible(true);
	}
	
	public void packAndResize() {
		this.setMinimumSize(null);
		this.pack();
		minimumSize.width = this.getWidth();
		minimumSize.height = this.getHeight();
		this.setMinimumSize(minimumSize);
		this.setLocation((resolution.width >> 1) - (this.getWidth() >> 1), (resolution.height >> 1) - (this.getHeight() >> 1));
	}
	
	private void initializeListeners() {
		new VMUpdater();

		vm_processes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					if(vm_processes.getSelectedValue() == null)
						return;
					attach(vm_processes.getSelectedValue());
				}
			}
		});
		
		attach.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(vm_processes.getSelectedValue() == null)
					return;
				attach(vm_processes.getSelectedValue());
			}
		});
	}
	
	public void addVM(String vmName) {
		vm_processes_md.addElement(vmName);
	}
	
	public void removeVM(Integer pid) {
		for(int i = 0; i < vm_processes_md.size(); i++) {
			String str = vm_processes_md.get(i);
			if(str.contains(" "))
				str = str.substring(0, str.indexOf(" "));
			if(pid == Integer.parseInt(str))
				vm_processes_md.remove(i);
		}
	}
	
	private void attach(final String name) {
		aw.setVisible(true);
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				int pid = VMList.getVMD(name.trim());
				if(pid == -1) {
					aw.setVisible(false);
					return;
				}
				
				try {
					Attacher.attach(pid);
				} catch(Exception e) {
					aw.setVisible(false);
				}
				
			}});
	}
	
	public void attached() {
		aw.setVisible(false);
		GUI.getInstance().setVisible(false);
	}

	
	@Override
	public void setVisible(boolean b) {
		if(!b)
			aw.setVisible(false);
		super.setVisible(b);
	}
	
}
