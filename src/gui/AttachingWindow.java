package gui;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

import tools.IconImage;

public class AttachingWindow extends JDialog {
	
	private static AttachingWindow _instance;
	public static AttachingWindow getInstance() {
		return _instance;
	}
	
	private JProgressBar progress = new JProgressBar();
	
	private JFrame frame;
	
	public AttachingWindow(JFrame frame) {
		super(frame);
		_instance = this;
		this.setIconImage(IconImage.getImage());
		this.frame = frame;
		this.setTitle("Progress...");
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		progress.setIndeterminate(true);
		this.add(progress);
		pack();
	}
	
	private void setPosition() {
		this.setLocation(frame.getLocation().x + (frame.getSize().width >> 1) - (this.getSize().width >> 1),
				frame.getLocation().y + (frame.getSize().height >> 1) - (this.getSize().height >> 1));
	}
	
	@Override
	public void setVisible(boolean b) {
		if(b)
			setPosition();
		super.setVisible(b);
	}
	
}
