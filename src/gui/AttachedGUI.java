package gui;

import tools.AgentRMI;
import tools.Attacher;
import tools.CommandExecutor;
import tools.IconImage;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AttachedGUI extends JFrame {

	private static AttachedGUI _instance;

	public static AttachedGUI getInstance() {
		return _instance == null ? _instance = new AttachedGUI() : _instance;
	}

	private AttachingWindow aw = new AttachingWindow(this);

	private JPanel classes = new JPanel();
	private JPanel jarDump = new JPanel();
	private JTextField jarPath = new JTextField(15);
	private JButton loc = new JButton("...");
	private JButton dumpJar = new JButton("Dump");

	private JPanel pathPanel = new JPanel();
	private JTextField path = new JTextField("C:/dump", 15);
	private JButton reloc = new JButton("...");
	private JButton doDump = new JButton("Dump");

	private JButton detach = new JButton("Detach");

	private JPanel makePanel = new JPanel();

	private JFileChooser fc = new JFileChooser();
	private JCheckBox pack = new JCheckBox("Pack  dumped files to jar");
	private JTextField name = new JTextField("dump.jar");

	private DefaultListModel<String> classes_md = new DefaultListModel<String>();
	private JList<String> classesList = new JList<String>(classes_md);

	private AttachedGUI() {
		this.setTitle("Class File Dumper");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setIconImage(IconImage.getImage());
		getContentPane().setLayout(new GridBagLayout());
		classesList
				.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		classesList.setLayoutOrientation(JList.VERTICAL);
		classesList.setVisibleRowCount(9);
		classesList.setFixedCellHeight(17);
		classes.setLayout(new BorderLayout(5, 5));
		classes.add(new JScrollPane(classesList), BorderLayout.NORTH);
		classes.add(doDump, BorderLayout.CENTER);
		classes.add(detach, BorderLayout.SOUTH);
		classes.setBorder(new TitledBorder("Loaded class list"));

		pathPanel.setLayout(new BorderLayout(5, 5));
		pathPanel.setBorder(new TitledBorder("Dump location"));
		pathPanel.add(path, BorderLayout.WEST);
		pathPanel.add(reloc, BorderLayout.EAST);

		makePanel.setLayout(new BorderLayout(5, 5));
		makePanel.setBorder(new TitledBorder("Make jar"));
		makePanel.add(pack, BorderLayout.NORTH);
		makePanel.add(name, BorderLayout.SOUTH);

		jarDump.setLayout(new BorderLayout(5, 5));
		jarDump.setBorder(new TitledBorder("Jar dump"));
		jarDump.add(jarPath, BorderLayout.WEST);
		jarDump.add(loc, BorderLayout.EAST);
		jarDump.add(dumpJar, BorderLayout.SOUTH);

		// (gridx, gridy, gridwidth, gridheight, weightx, weighty, anchor, fill,
		// insets, ipadx, ipady)
		getContentPane().add(
				classes,
				new GridBagConstraints(0, 0, 1, 4, 1, 1,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.VERTICAL, new Insets(4, 4, 0, 0), 0,
						0));
		getContentPane().add(
				pathPanel,
				new GridBagConstraints(1, 0, 1, 1, 1, 0.1,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(4, 4, 0, 0),
						0, 0));
		getContentPane().add(
				jarDump,
				new GridBagConstraints(1, 1, 1, 1, 1, 1,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(4, 4, 0, 0),
						0, 0));

		getContentPane().add(
				makePanel,
				new GridBagConstraints(1, 2, 1, 1, 1, 1,
						GridBagConstraints.NORTHWEST,
						GridBagConstraints.HORIZONTAL, new Insets(4, 4, 0, 0),
						0, 0));


		initializeListeners();
	}

	private void initializeListeners() {
		doDump.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				List<String> classes = classesList.getSelectedValuesList();
				new CommandExecutor(CommandExecutor.CE.CLASSES, path.getText().trim(), name.getText().trim(),
						pack.isSelected(), null, classes);
			}
		});

		reloc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				File fp = showDialog();
				if (fp != null)
					path.setText(fp.getAbsolutePath());
			}
		});

		detach.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						try {
							Attacher.detach();
						} catch (IOException e) {
						}
						AttachedGUI.getInstance().setVisible(false);
						GUI.getInstance().setVisible(true);
					}
				});
			}
		});

		loc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				File fp = showDialog();
				if (fp != null) {
					String path = fp.getAbsolutePath();
					jarPath.setText(path);
					try {
						name.setText(fp.getAbsolutePath().substring(path.lastIndexOf(File.separator) + 1, path.length()));
					} catch (Exception e1) {
					}
				}
			}
		});

		dumpJar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new CommandExecutor(CommandExecutor.CE.JAR, path.getText().trim(), name.getText().trim(),
						pack.isSelected(), jarPath.getText().trim(), null);
			}
		});
	}

	public void draw() {
		packAndResize();
		classes_md.clear();
		AgentRMI.getInstance().connect(new Runnable() {

			@Override
			public void run() {
				List<String> set = new ArrayList<String>();
				try {
					for (String e : AgentRMI.getInstance().getLoadedClasses()) {
						if (!e.contains(".") || e.contains("classdumper"))
							continue;
						String pcg = e.substring(0, e.lastIndexOf(".")) + ".*";
						String root = pcg.substring(0, pcg.indexOf(".")) + ".*";
						if (!set.contains(pcg))
							set.add(pcg);
						if (!set.contains(root))
							set.add(root);

					}
				} catch (RemoteException e) {
				}
				Collections.sort(set);
				for (int i = 0; i < set.size(); i++)
					classes_md.add(i, set.get(i));
				classes_md.add(0, "*");
			}
		});
		GUI.getInstance().setVisible(false);
		this.setVisible(true);
	}

	public void packAndResize() {
		this.setMinimumSize(null);
		this.pack();
		GUI.minimumSize.width = this.getWidth();
		GUI.minimumSize.height = this.getHeight();
		this.setMinimumSize(GUI.minimumSize);
		this.setLocation((GUI.resolution.width >> 1) - (this.getWidth() >> 1),
				(GUI.resolution.height >> 1) - (this.getHeight() >> 1));
	}

	private File showDialog() {
		fc.setDialogType(JFileChooser.OPEN_DIALOG);
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int ret = fc.showDialog(null, "Select");
		if (ret == JFileChooser.APPROVE_OPTION) {
			return fc.getSelectedFile();
		} else
			return null;
	}

}
