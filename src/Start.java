

import gui.GUI;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import tools.Attacher;

public class Start {
	
	public static void main(String[] args) throws InvocationTargetException, InterruptedException {
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception e) {}
			
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					GUI.getInstance();
				}
			});
	}
	
	private static class ShutdownHook extends Thread {
		@Override
		public void run() {
			try {
				Attacher.detach();
			} catch (IOException e) {
			}
		}
	}

}
